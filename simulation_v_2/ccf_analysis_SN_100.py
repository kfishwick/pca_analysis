import numpy as np
import matplotlib.pyplot as plt
from ccf_interpolate import ccf

nudot_matrix = np.loadtxt("nudot_matrix_20_trans_125_cad_.dat")
half_nudot_matrix = np.loadtxt("nudot_matrix_reduced_2_20_trans_125_cad_.dat")
quarter_nudot_matrix = np.loadtxt("nudot_matrix_reduced_4_20_trans_125_cad_.dat")
eigth_nudot_matrix = np.loadtxt("nudot_matrix_reduced_8_20_trans_125_cad_.dat")
sixteenth_nudot_matrix = np.loadtxt("nudot_matrix_reduced_16_20_trans_125_cad_.dat")

coeff_matrix = np.loadtxt("coeff_matrix_20_trans_125_cad_SN_100.dat")
half_coeff_matrix = np.loadtxt("coeff_matrix_reduced_2_20_trans_125_cad_SN_100.dat")
quarter_coeff_matrix = np.loadtxt("coeff_matrix_reduced_4_20_trans_125_cad_SN_100.dat")
eigth_coeff_matrix = np.loadtxt("coeff_matrix_reduced_8_20_trans_125_cad_SN_100.dat")
sixteenth_coeff_matrix = np.loadtxt("coeff_matrix_reduced_16_20_trans_125_cad_SN_100.dat")

def check_mjd_spacing(mjds, name):
	#mjds = mjds.sort()
	mjd_steps = np.array(())
	for i in range(len(mjds)-1):
		mjd_steps = np.append(mjd_steps, mjds[i+1]-mjds[i])
	average = np.mean(mjd_steps)
	print(average)
	plt.figure()
	plt.hist(mjd_steps, bins =np.arange(153))
	plt.axvline(x=average, ymin =0,ymax = 1, color = "k", linewidth = 0.5)
	plt.xlabel("time between observations (days)")
	plt.ylabel("freq.")
	plt.savefig(name)
	plt.show()

def plot_all_mjd_spacing():
	check_mjd_spacing(half_nudot_matrix[:, 0], "half_removed_mjd_analysis.png")
	check_mjd_spacing(quarter_nudot_matrix[:, 0], "quarter_removed_mjd_analysis.png")
	check_mjd_spacing(eigth_nudot_matrix[:, 0], "eigth_removed_mjd_analysis.png")
	check_mjd_spacing(sixteenth_nudot_matrix[:, 0], "sixteenth_removed_mjd_analysis.png")


def do_ccf(kernel, full_nudot, half_nudot, quarter_nudot, eigth_nudot, sixteenth_nudot, full_coeff, half_coeff, quarter_coeff, eigth_coeff, sixteenth_coeff, lags):
	#do ccf
	ccf_full = ccf(full_nudot[:,1], full_coeff[:,1], full_nudot[:,0], full_nudot[:,0], method = kernel, lags=lags, min_weighting=10, max_gap =40).results()
	ccf_half = ccf(half_nudot[:,1], half_coeff[:,1], half_nudot[:,0], half_nudot[:,0], method = kernel, lags=lags, min_weighting=10, max_gap =40).results()
	ccf_quarter = ccf(quarter_nudot[:,1], quarter_coeff[:,1], quarter_nudot[:,0], quarter_nudot[:,0], method = kernel, lags=lags, min_weighting=10, max_gap =40).results()
	ccf_eigth = ccf(eigth_nudot[:,1], eigth_coeff[:,1], eigth_nudot[:,0], eigth_nudot[:,0], method = kernel, lags=lags, min_weighting=10, max_gap =40).results()
	ccf_sixteenth = ccf(sixteenth_nudot[:,1], sixteenth_coeff[:,1], sixteenth_nudot[:,0], sixteenth_nudot[:,0], method = kernel, lags=lags, min_weighting=10, max_gap =40).results()
	
	#save results
	name = kernel + "_ccf_full_SN_100.dat"
	np.savetxt(name, ccf_full)
	name = kernel + "_ccf_half_SN_100.dat"
	np.savetxt(name, ccf_half)
	name = kernel + "_ccf_quarter_SN_100.dat"
	np.savetxt(name, ccf_quarter)
	name = kernel + "_ccf_eigth_SN_100.dat"
	np.savetxt(name, ccf_eigth)
	name = kernel + "_ccf_sixteenth_SN_100.dat"
	np.savetxt(name, ccf_sixteenth)

def make_lags(lag_number):
		# lags = delta_t * k - k is an int, delta_t is constant#
		
		x = int(lag_number/2)
		k = range(-x,x+1,8)
		delta_t = 1
		lags = np.array(())
		for n in k:
			lags = np.append(lags, n*delta_t)
		
		return lags
#do ccf 
#make lags
lags = make_lags(2000)

#square kernel
do_ccf("rectangle", nudot_matrix, half_nudot_matrix, quarter_nudot_matrix, eigth_nudot_matrix, sixteenth_nudot_matrix, coeff_matrix, half_coeff_matrix, quarter_coeff_matrix, eigth_coeff_matrix, sixteenth_coeff_matrix, lags)

#triangle kernel
do_ccf("triangle", nudot_matrix, half_nudot_matrix, quarter_nudot_matrix, eigth_nudot_matrix, sixteenth_nudot_matrix, coeff_matrix, half_coeff_matrix, quarter_coeff_matrix, eigth_coeff_matrix, sixteenth_coeff_matrix, lags)

#sinc kernel
do_ccf("sinc", nudot_matrix, half_nudot_matrix, quarter_nudot_matrix, eigth_nudot_matrix, sixteenth_nudot_matrix, coeff_matrix, half_coeff_matrix, quarter_coeff_matrix, eigth_coeff_matrix, sixteenth_coeff_matrix, lags)

#gaussian
do_ccf("gaussian", nudot_matrix, half_nudot_matrix, quarter_nudot_matrix, eigth_nudot_matrix, sixteenth_nudot_matrix, coeff_matrix, half_coeff_matrix, quarter_coeff_matrix, eigth_coeff_matrix, sixteenth_coeff_matrix, lags)
