import numpy as np
import matplotlib.pyplot as plt
from ccf_sinc_no_const import ccf

nudot_matrix = np.loadtxt("nudot_matrix_20_trans_125_cad_error_666.dat")
nudot_matrix_weekly = np.loadtxt("nudot_matrix_reduced_weekly_20_trans_125_cad_error_666.dat")
nudot_matrix_monthly = np.loadtxt("nudot_matrix_reduced_monthly_20_trans_125_cad_error_666.dat")
nudot_matrix_125 = np.loadtxt("nudot_matrix_reduced_125_20_trans_125_cad_error_666.dat")


coeff_matrix = np.loadtxt("coeff_matrix_20_trans_125_cad_error_666.dat")
coeff_matrix_weekly = np.loadtxt("coeff_matrix_20_trans_125_cad_error_666_weekly.dat")
coeff_matrix_monthly = np.loadtxt("coeff_matrix_20_trans_125_cad_error_666_monthly.dat")
coeff_matrix_125 = np.loadtxt("coeff_matrix_20_trans_125_cad_error_666_125.dat")

def check_mjd_spacing(mjds, name):
	#mjds = mjds.sort()
	mjd_steps = np.array(())
	for i in range(len(mjds)-1):
		mjd_steps = np.append(mjd_steps, mjds[i+1]-mjds[i])
	average = np.mean(mjd_steps)
	print(average)
	plt.figure()
	plt.hist(mjd_steps, bins =np.arange(153))
	plt.axvline(x=average, ymin =0,ymax = 1, color = "k", linewidth = 0.5)
	plt.xlabel("time between observations (days)")
	plt.ylabel("freq.")
	plt.savefig("no_const_" + name)
	plt.show()

def plot_all_mjd_spacing():
	check_mjd_spacing(half_nudot_matrix[:, 0], "half_removed_mjd_analysis.png")
	check_mjd_spacing(quarter_nudot_matrix[:, 0], "quarter_removed_mjd_analysis.png")
	check_mjd_spacing(eigth_nudot_matrix[:, 0], "eigth_removed_mjd_analysis.png")
	check_mjd_spacing(sixteenth_nudot_matrix[:, 0], "sixteenth_removed_mjd_analysis.png")



def do_ccf(kernel,name, weekly_nudot, monthly_nudot, nudot_125, weekly_coeff, monthly_coeff, coeff_125,  lags,max_gap ,h):
	#do ccf
	print("weekly")
	ccf_weekly = ccf(weekly_nudot[:,1], weekly_coeff[:,1], weekly_nudot[:,0], weekly_coeff[:,0], method = kernel, lags=lags, min_weighting=0, max_gap =max_gap, bin_width = 7*h).results()
	print("monthly")
	ccf_monthly = ccf(monthly_nudot[:,1], monthly_coeff[:,1], monthly_nudot[:,0], monthly_coeff[:,0], method = kernel, lags=lags, min_weighting=0, max_gap =max_gap, bin_width = 30*h).results()
	print("125 days")
	ccf_125 = ccf(nudot_125[:,1], coeff_125[:,1], nudot_125[:,0], coeff_125[:,0], method = kernel, lags=lags, min_weighting=0, max_gap =max_gap, bin_width = 125*h).results()
	

	file_name = kernel + name +"ccf_error_666_weekly.dat"
	np.savetxt(file_name, ccf_weekly)

	file_name = kernel + name +"ccf_error_666_monthly.dat"
	np.savetxt(file_name, ccf_monthly)

	file_name = kernel + name + "ccf_error_666_125.dat"
	np.savetxt(file_name, ccf_125)

def make_lags(lag_number):
		# lags = delta_t * k - k is an int, delta_t is constant#
		
		x = int(lag_number/2)
		k = range(-x,x+1,8)
		delta_t = 1
		lags = np.array(())
		for n in k:
			lags = np.append(lags, n*delta_t)
		
		return lags


#check_mjd_spacing(coeff_matrix_weekly[:,0], "weekly")
#check_mjd_spacing(coeff_matrix_monthly[:,0], "monthly")
#check_mjd_spacing(coeff_matrix_125[:,0], "125")

#do ccf 
#make lags
lags = make_lags(2000)
"""
print("standard")
ccf_standard = ccf(nudot_matrix[:,1], coeff_matrix[:,1], nudot_matrix[:,0], coeff_matrix[:,0], method = "standard", lags=lags, min_weighting=0, max_gap =40, confidence=1e-5).results()
np.savetxt("standard_ccf_error_666.dat", ccf_standard)

print("rectangle")
do_ccf("rectangle","_double_h_", nudot_matrix_weekly, nudot_matrix_monthly, nudot_matrix_125, coeff_matrix_weekly, coeff_matrix_monthly, coeff_matrix_125,  lags,0, 1)

print("triangle")
do_ccf("triangle","_double_h_", nudot_matrix_weekly, nudot_matrix_monthly, nudot_matrix_125, coeff_matrix_weekly, coeff_matrix_monthly, coeff_matrix_125,  lags,0, 1)

print("gaussian")

do_ccf("gaussian", nudot_matrix_weekly, nudot_matrix_monthly, nudot_matrix_125, coeff_matrix_weekly, coeff_matrix_monthly, coeff_matrix_125,  lags)
"""
do_ccf("gaussian","_double_h_", nudot_matrix_weekly, nudot_matrix_monthly, nudot_matrix_125, coeff_matrix_weekly, coeff_matrix_monthly, coeff_matrix_125,  lags,0, 1/2)


print("sinc")
do_ccf("sinc","_double_h_" ,nudot_matrix_weekly, nudot_matrix_monthly, nudot_matrix_125, coeff_matrix_weekly, coeff_matrix_monthly, coeff_matrix_125,  lags,0, 2)




