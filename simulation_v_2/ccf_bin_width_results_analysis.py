import numpy as np
import matplotlib.pyplot as plt

standard = np.loadtxt("standard_ccf_error_666.dat")

rec_weekly = np.loadtxt("rectangleccf_error_666_weekly.dat")
rec_monthly = np.loadtxt("rectangleccf_error_666_monthly.dat")
rec_125 = np.loadtxt("rectangleccf_error_666_125.dat")

rec_weekly_half_h = np.loadtxt("rectangle_half_h_ccf_error_666_weekly.dat")
rec_monthly_half_h = np.loadtxt("rectangle_half_h_ccf_error_666_monthly.dat")
rec_125_half_h = np.loadtxt("rectangle_half_h_ccf_error_666_125.dat")

rec_weekly_double_h = np.loadtxt("rectangle_double_h_ccf_error_666_weekly.dat")
rec_monthly_double_h = np.loadtxt("rectangle_double_h_ccf_error_666_monthly.dat")
rec_125_double_h = np.loadtxt("rectangle_double_h_ccf_error_666_125.dat")

tri_weekly = np.loadtxt("triangleccf_error_666_weekly.dat")
tri_monthly = np.loadtxt("triangleccf_error_666_monthly.dat")
tri_125 = np.loadtxt("triangleccf_error_666_125.dat")

tri_weekly_half_h = np.loadtxt("triangle_half_h_ccf_error_666_weekly.dat")
tri_monthly_half_h = np.loadtxt("triangle_half_h_ccf_error_666_monthly.dat")
tri_125_half_h = np.loadtxt("triangle_half_h_ccf_error_666_125.dat")

tri_weekly_double_h = np.loadtxt("triangle_double_h_ccf_error_666_weekly.dat")
tri_monthly_double_h = np.loadtxt("triangle_double_h_ccf_error_666_monthly.dat")
tri_125_double_h = np.loadtxt("triangle_double_h_ccf_error_666_125.dat")

gaussian_weekly = np.loadtxt("gaussianccf_error_666_weekly.dat")
gaussian_monthly = np.loadtxt("gaussianccf_error_666_monthly.dat")
gaussian_125 = np.loadtxt("gaussianccf_error_666_125.dat")

gaussian_weekly_half_h = np.loadtxt("gaussian_half_h_ccf_error_666_weekly.dat")
gaussian_monthly_half_h = np.loadtxt("gaussian_half_h_ccf_error_666_monthly.dat")
gaussian_125_half_h = np.loadtxt("gaussian_half_h_ccf_error_666_125.dat")

gaussian_weekly_double_h = np.loadtxt("gaussian_double_h_ccf_error_666_weekly.dat")
gaussian_monthly_double_h = np.loadtxt("gaussian_double_h_ccf_error_666_monthly.dat")
gaussian_125_double_h = np.loadtxt("gaussian_double_h_ccf_error_666_125.dat")


sinc_weekly = np.loadtxt("sincccf_error_666_weekly.dat")
sinc_monthly = np.loadtxt("sincccf_error_666_monthly.dat")
sinc_125 = np.loadtxt("sincccf_error_666_125.dat")

sinc_weekly_half_h = np.loadtxt("sinc_half_h_ccf_error_666_weekly.dat")
sinc_monthly_half_h = np.loadtxt("sinc_half_h_ccf_error_666_monthly.dat")
sinc_125_half_h = np.loadtxt("sinc_half_h_ccf_error_666_125.dat")

sinc_weekly_double_h = np.loadtxt("sinc_double_h_ccf_error_666_weekly.dat")
sinc_monthly_double_h = np.loadtxt("sinc_double_h_ccf_error_666_monthly.dat")
sinc_125_double_h = np.loadtxt("sinc_double_h_ccf_error_666_125.dat")


def make_resids(standard, rec, tri, gauss, sinc):
		
	rec_ccf_resid=rec[:,1]-standard[:,1]
	rec_error_resid = np.sqrt(rec[:,2]**2+standard[:,2]**2)
	tri_ccf_resid=tri[:,1]-standard[:,1]
	tri_error_resid = np.sqrt(tri[:,2]**2+standard[:,2]**2)
	gauss_ccf_resid=gauss[:,1]-standard[:,1]
	gauss_error_resid = np.sqrt(gauss[:,2]**2+standard[:,2]**2)
	sinc_ccf_resid=sinc[:,1]-standard[:,1]
	sinc_error_resid = np.sqrt(sinc[:,2]**2+standard[:,2]**2)	

	
	rec_resids = np.column_stack((rec[:,0], rec_ccf_resid, rec_error_resid, rec[:,3]))
	tri_resids = np.column_stack((tri[:,0], tri_ccf_resid, tri_error_resid, tri[:,3]))
	gauss_resids = np.column_stack((gauss[:,0], gauss_ccf_resid, gauss_error_resid, gauss[:,3]))
	sinc_resids = np.column_stack((sinc[:,0], sinc_ccf_resid, sinc_error_resid, sinc[:,3]))

	return rec_resids, tri_resids, gauss_resids, sinc_resids

def make_resids_bin_width(standard, normal, half, double):
		
	norm_ccf_resid=normal[:,1]-standard[:,1]
	norm_error_resid = np.sqrt(normal[:,2]**2+standard[:,2]**2)
	half_ccf_resid=half[:,1]-standard[:,1]
	half_error_resid = np.sqrt(half[:,2]**2+standard[:,2]**2)
	double_ccf_resid=double[:,1]-standard[:,1]
	double_error_resid = np.sqrt(double[:,2]**2+standard[:,2]**2)	

	
	norm_resids = np.column_stack((normal[:,0], norm_ccf_resid, norm_error_resid, normal[:,3]))
	half_resids = np.column_stack((half[:,0], half_ccf_resid, half_error_resid, half[:,3]))
	double_resids = np.column_stack((double[:,0], double_ccf_resid, double_error_resid, double[:,3]))
	return norm_resids, half_resids, double_resids
"""
def plot_ccf(standard, rec, tri, gauss, sinc, name):
	fig,(ax_1, ax_2, ax_3, ax_4, ax_5) = plt.subplots(5,sharex=True)
	y_tick_label_size = 5
	ax_1.errorbar(standard[:,0],standard[:,1], standard[:,2])
	ax_1.set_ylabel("standard")
	plt.setp(ax_1.get_yticklabels(), fontsize=y_tick_label_size)
	
	ax_2.errorbar(rec[:,0],rec[:,1], rec[:,2])
	ax_2.set_ylabel("rectangle")
	ax_2.text(np.max(rec[:,0])-600, np.min(rec[:,1])*(0.9), r"$\langle b \rangle$ = {:.1f}, $\sigma_b$ = {:.1f}".format(np.mean(rec[:,3]), np.std(rec[:,3])))
	plt.setp(ax_2.get_yticklabels(), fontsize=y_tick_label_size)

	ax_3.errorbar(tri[:,0],tri[:,1], tri[:,2])
	ax_3.set_ylabel("triangle")
	plt.setp(ax_3.get_yticklabels(), fontsize=y_tick_label_size)
	ax_3.text(np.max(tri[:,0])-600, np.min(tri[:,1])*(0.9), r"$\langle b \rangle$ = {:.1f}, $\sigma_b$ = {:.1f}".format(np.mean(tri[:,3]), np.std(tri[:,3])))

	ax_4.errorbar(gauss[:,0],gauss[:,1], gauss[:,2])
	ax_4.set_ylabel("gaussian")
	plt.setp(ax_4.get_yticklabels(), fontsize=y_tick_label_size)
	ax_4.text(np.max(gauss[:,0])-600, np.min(gauss[:,1])*(0.9), r"$\langle b \rangle$ = {:.1f}, $\sigma_b$ = {:.1f}".format(np.mean(gauss[:,3]), np.std(gauss[:,3])))

	ax_5.errorbar(sinc[:,0],sinc[:,1], sinc[:,2])
	ax_5.set_ylabel("sinc")
	plt.setp(ax_5.get_yticklabels(), fontsize=y_tick_label_size)
	ax_5.text(np.max(sinc[:,0])-600, np.min(sinc[:,1])*(0.9), r"$\langle b \rangle$ = {:.1f}, $\sigma_b$ = {:.1f}".format(np.mean(sinc[:,3]), np.std(sinc[:,3])))

	ax_5.set_xlabel("Lags (days)")
	plt.subplots_adjust(hspace=.0)
	plt.savefig("ccf_results_"+name+".png")
	plt.show()
	
	rec_resids, tri_resids, gauss_resids, sinc_resids = make_resids(standard, rec, tri, gauss, sinc)

	fig,(ax_1, ax_2, ax_3, ax_4, ax_5) = plt.subplots(5,sharex=True)
	y_tick_label_size = 5
	ax_1.errorbar(standard[:,0],standard[:,1], standard[:,2])
	ax_1.set_ylabel("standard")
	plt.setp(ax_1.get_yticklabels(), fontsize=y_tick_label_size)
	
	ax_2.errorbar(rec_resids[:,0],rec_resids[:,1], rec_resids[:,2])
	ax_2.set_ylabel("rectangle")
	plt.setp(ax_2.get_yticklabels(), fontsize=y_tick_label_size)

	ax_3.errorbar(tri_resids[:,0],tri_resids[:,1], tri_resids[:,2])
	ax_3.set_ylabel("triangle")
	plt.setp(ax_3.get_yticklabels(), fontsize=y_tick_label_size)

	ax_4.errorbar(gauss_resids[:,0],gauss_resids[:,1], gauss_resids[:,2])
	ax_4.set_ylabel("gaussian")
	plt.setp(ax_4.get_yticklabels(), fontsize=y_tick_label_size)

	ax_5.errorbar(sinc_resids[:,0],sinc_resids[:,1], sinc_resids[:,2])
	ax_5.set_ylabel("sinc")
	plt.setp(ax_5.get_yticklabels(), fontsize=y_tick_label_size)

	ax_5.set_xlabel("Lags (days)")
	plt.subplots_adjust(hspace=.0)
	plt.savefig("ccf_resids_"+name+".png")
	plt.show()

"""
def divide_resids(resids):
	one_sigma = np.where(np.abs(resids[:,1])<=resids[:,2])[0]
	not_one_sigma = np.where(resids[:,1]>resids[:,2])[0]
	return resids[one_sigma,:], resids[one_sigma,:]
def plot_ccf(standard, rec, tri, gauss, sinc,name):
	rec_resids, tri_resids, gauss_resids, sinc_resids = make_resids(standard, rec, tri, gauss, sinc)
	fig,(ax_1, ax_2, ax_3, ax_4) = plt.subplots(4,sharex=True)
	y_tick_label_size = 5
	ax_1.errorbar(standard[:,0],standard[:,1], standard[:,2])
	ax_1.set_ylabel("standard")
	plt.setp(ax_1.get_yticklabels(), fontsize=y_tick_label_size)

	
	ax_2.errorbar(rec[:,0],rec[:,1], rec[:,2], label = "CCF")
	ax_2.errorbar(rec_resids[:,0],rec_resids[:,1], rec_resids[:,2], label = "CCF residuals")
	ax_2.legend(bbox_to_anchor =(0.65, 1.25))
	ax_2.set_ylabel("rectangle")
	#ax_2.text(np.max(rec[:,0])-600, np.min(rec[:,1])*(1), r"$\langle b \rangle$ = {:.1f}, $\sigma_b$ = {:.1f}".format(np.mean(rec[:,3]), np.std(rec[:,3])))
	plt.setp(ax_2.get_yticklabels(), fontsize=y_tick_label_size)
	print("rectangle- mean: {}, std: {}".format(np.mean(rec[:,3]), np.std(rec[:,3])))

	ax_3.errorbar(tri[:,0],tri[:,1], tri[:,2])
	ax_3.errorbar(tri_resids[:,0],tri_resids[:,1], tri_resids[:,2])
	ax_3.set_ylabel("triangle")
	plt.setp(ax_3.get_yticklabels(), fontsize=y_tick_label_size)
	#ax_3.text(np.max(tri[:,0])-600, np.min(tri[:,1])*(1), r"$\langle b \rangle$ = {:.1f}, $\sigma_b$ = {:.1f}".format(np.mean(tri[:,3]), np.std(tri[:,3])))
	print("triangle- mean: {}, std: {}".format(np.mean(tri[:,3]), np.std(tri[:,3])))

	ax_4.errorbar(gauss[:,0],gauss[:,1], gauss[:,2])
	ax_4.errorbar(gauss_resids[:,0],gauss_resids[:,1], gauss_resids[:,2])
	ax_4.set_ylabel("gaussian")
	plt.setp(ax_4.get_yticklabels(), fontsize=y_tick_label_size)
	#ax_4.text(np.max(gauss[:,0])-600, np.min(gauss[:,1])*(1), r"$\langle b \rangle$ = {:.1f}, $\sigma_b$ = {:.1f}".format(np.mean(gauss[:,3]), np.std(gauss[:,3])))
	print("gaussian- mean: {}, std: {}".format(np.mean(gauss[:,3]), np.std(gauss[:,3])))
	'''
	ax_5.errorbar(sinc[:,0],sinc[:,1], sinc[:,2])
	ax_5.errorbar(sinc_resids[:,0],sinc_resids[:,1], sinc_resids[:,2])
	ax_5.set_ylabel("sinc")
	plt.setp(ax_5.get_yticklabels(), fontsize=y_tick_label_size)
	ax_5.text(np.max(sinc[:,0])-600, np.min(sinc[:,1])*(1), r"$\langle b \rangle$ = {:.1f}, $\sigma_b$ = {:.1f}".format(np.mean(sinc[:,3]), np.std(sinc[:,3])))
	'''
	ax_4.set_xlabel("Lags (days)")
	plt.subplots_adjust(hspace=.0)
	plt.savefig("ccf_results_resids_"+name+".png")
	plt.show()
def calc_rmse(resids):
	return np.sqrt(np.mean(resids[:,1]**2)), resids[0,2]/np.sqrt(len(resids[:,2]))



def plot_rmse(rec_rms, rec_rms_error, tri_rms,tri_rms_error, gauss_rms, gauss_rms_error, sinc_rms, sinc_rms_error, title):
	bar_width = 0.3
	x_tick_label = ["Weekly", "Monthly", "Nyquist"]
	bar_rec = np.array((0,1,2))
	bar_tri = bar_rec+bar_width
	bar_gauss = bar_rec+2*bar_width
	#bar_sinc = bar_rec+3*bar_width
	plt.bar(bar_rec, rec_rms, bar_width, yerr = rec_rms_error, label = "Rectangle",error_kw=dict(lw=1, capsize=11, capthick=1))
	plt.bar(bar_tri, tri_rms, bar_width, yerr = tri_rms_error, label = "Triangle", error_kw=dict(lw=1, capsize=11, capthick=1))
	plt.bar(bar_gauss, gauss_rms, bar_width, yerr = gauss_rms_error, label = "Gaussian", error_kw=dict(lw=1, capsize=11, capthick=1))
	#plt.bar(bar_sinc, sinc_rms, bar_width, yerr = sinc_rms_error, label = "Sinc",error_kw=dict(lw=1, capsize=11, capthick=1))
	plt.legend()
	plt.xlabel("Sampling")
	plt.ylabel("RMSE")
	plt.xticks(bar_tri, x_tick_label)
	plt.savefig(title)
	plt.show()

def analyse_bin_width(standard, weekly, weekly_half, weekly_double, monthly, monthly_half, monthly_double, nyquist, nyquist_half, nyquist_double,name):
	weekly_resids, weekly_half_resids, weekly_double_resids = make_resids_bin_width(standard, weekly,weekly_half, weekly_double)
	monthly_resids, monthly_half_resids, monthly_double_resids = make_resids_bin_width(standard, monthly, monthly_half, monthly_double)
	nyquist_resids, nyquist_half_resids, nyquist_double_resids = make_resids_bin_width(standard, nyquist, nyquist_half, nyquist_double)
	
	norm_rms = np.array((calc_rmse(weekly_resids),calc_rmse(monthly_resids), calc_rmse(nyquist_resids))) 
	half_rms = np.array((calc_rmse(weekly_half_resids),calc_rmse(monthly_half_resids), calc_rmse(nyquist_half_resids))) 
	double_rms = np.array((calc_rmse(weekly_double_resids),calc_rmse(monthly_double_resids), calc_rmse(nyquist_double_resids))) 
	bar_width = 0.3
	x_tick_label = ["Weekly", "Monthly", "Nyquist"]
	bar_norm = np.array((0,1,2))
	bar_half = bar_norm+bar_width
	bar_double = bar_norm+2*bar_width
	plt.bar(bar_norm, norm_rms[:,0], bar_width, yerr = norm_rms[:,1], label = "standard",error_kw=dict(lw=1, capsize=11, capthick=1))
	plt.bar(bar_half, half_rms[:,0], bar_width, yerr = half_rms[:,1], label = "half standard", error_kw=dict(lw=1, capsize=11, capthick=1))
	plt.bar(bar_double, double_rms[:,0], bar_width, yerr = double_rms[:,1], label = "double standard", error_kw=dict(lw=1, capsize=11, capthick=1))
	plt.legend()
	plt.xlabel("Sampling")
	plt.ylabel("RMSE")
	plt.xticks(bar_half, x_tick_label)
	plt.savefig(name)
	plt.show()
	
def find_zero_lag_value(data):
	arg = np.where(data[:,0]==0)[0]
	print("{} pm {}".format(data[arg,1], data[arg,2]))
print("control")
find_zero_lag_value(standard)

print("gaussian weekly")
find_zero_lag_value(gaussian_weekly)

print("gaussian monthly")
find_zero_lag_value(gaussian_monthly)


print("gaussian nyquist")
find_zero_lag_value(gaussian_125)

print("rec weekly")
find_zero_lag_value(rec_weekly)

print("rec monthly")
find_zero_lag_value(rec_monthly)


print("rec nyquist")
find_zero_lag_value(rec_125)

print("tri weekly")
find_zero_lag_value(tri_weekly)

print("tri monthly")
find_zero_lag_value(tri_monthly)

print("tri nyquist")
find_zero_lag_value(tri_125)

print("weekly")
plot_ccf(standard, rec_weekly, tri_weekly, gaussian_weekly, sinc_weekly,"weekly")
print("monthly")
plot_ccf(standard, rec_monthly, tri_monthly, gaussian_monthly, sinc_monthly, "monthly")
print("Nyquist")
plot_ccf(standard, rec_125, tri_125, gaussian_125, sinc_125, "125")
print("weekly_half_h")
plot_ccf(standard, rec_weekly_half_h, tri_weekly_half_h, gaussian_weekly_half_h, sinc_weekly_half_h,"weekly_half_h")
print("weekly_double_h")
plot_ccf(standard, rec_weekly_double_h, tri_weekly_double_h, gaussian_weekly_double_h, sinc_weekly_double_h, "weekly_double_h")
print("monthly_half_h")
plot_ccf(standard, rec_monthly_half_h, tri_monthly_half_h, gaussian_monthly_half_h, sinc_monthly_half_h, "monthly_half_h")
print("monthly_double_h")
plot_ccf(standard, rec_monthly_double_h, tri_monthly_double_h, gaussian_monthly_double_h, sinc_monthly_double_h, "monthly_double_h")
print("Nyquist_half_h")
plot_ccf(standard, rec_125_half_h, tri_125_half_h, gaussian_125_half_h, sinc_125_half_h, "125_half_h")
print("Nyquist_double_h")
plot_ccf(standard, rec_125_double_h, tri_125_double_h, gaussian_125_double_h, sinc_125_double_h, "125_double_h")

rec_resids_weekly, tri_resids_weekly, gauss_resids_weekly, sinc_resids_weekly = make_resids(standard, rec_weekly, tri_weekly, gaussian_weekly, sinc_weekly)

rec_resids_monthly, tri_resids_monthly, gauss_resids_monthly, sinc_resids_monthly = make_resids(standard, rec_monthly, tri_monthly, gaussian_monthly, sinc_monthly)

rec_resids_125, tri_resids_125, gauss_resids_125, sinc_resids_125 = make_resids(standard, rec_125, tri_125, gaussian_125, sinc_125)
 
rec_rms_weekly, rec_rms_error_weekly = calc_rmse(rec_resids_weekly)
rec_rms_monthly, rec_rms_error_monthly = calc_rmse(rec_resids_monthly)
rec_rms_125, rec_rms_error_125 = calc_rmse(rec_resids_125)

tri_rms_weekly, tri_rms_error_weekly = calc_rmse(tri_resids_weekly)
tri_rms_monthly, tri_rms_error_monthly = calc_rmse(tri_resids_monthly)
tri_rms_125, tri_rms_error_125 = calc_rmse(tri_resids_125)

gauss_rms_weekly, gauss_rms_error_weekly = calc_rmse(gauss_resids_weekly)
gauss_rms_monthly, gauss_rms_error_monthly = calc_rmse(gauss_resids_monthly)
gauss_rms_125, gauss_rms_error_125 = calc_rmse(gauss_resids_125)

sinc_rms_weekly, sinc_rms_error_weekly = calc_rmse(sinc_resids_weekly)
sinc_rms_monthly, sinc_rms_error_monthly = calc_rmse(sinc_resids_monthly)
sinc_rms_125, sinc_rms_error_125 = calc_rmse(sinc_resids_125)

rec_rms = np.array((rec_rms_weekly, rec_rms_monthly, rec_rms_125))
rec_rms_error = np.array((rec_rms_error_weekly, rec_rms_error_monthly, rec_rms_error_125))

tri_rms = np.array((tri_rms_weekly,tri_rms_monthly, tri_rms_125))
tri_rms_error = np.array((tri_rms_error_weekly,tri_rms_error_monthly, tri_rms_error_125))

gauss_rms = np.array((gauss_rms_weekly,gauss_rms_monthly, gauss_rms_125))
gauss_rms_error = np.array((gauss_rms_error_weekly,gauss_rms_error_monthly, gauss_rms_error_125))

sinc_rms = np.array((sinc_rms_weekly,sinc_rms_monthly, sinc_rms_125))
sinc_rms_error = np.array((sinc_rms_error_weekly,sinc_rms_error_monthly, sinc_rms_error_125))

plot_rmse(rec_rms, rec_rms_error, tri_rms,tri_rms_error, gauss_rms, gauss_rms_error, sinc_rms, sinc_rms_error, "CCF_rmse_error_666.png")
print("gaussian")
analyse_bin_width(standard, gaussian_weekly,gaussian_weekly_half_h, gaussian_weekly_double_h, gaussian_monthly, gaussian_monthly_half_h, gaussian_monthly_double_h, gaussian_125,gaussian_125_half_h, gaussian_125_double_h, "gaussian_ccf_rmse_bin_widths.png")
print("rectangle")
analyse_bin_width(standard, rec_weekly,rec_weekly_half_h, rec_weekly_double_h, rec_monthly, rec_monthly_half_h, rec_monthly_double_h, rec_125,rec_125_half_h, rec_125_double_h, "rectangle_ccf_rmse_bin_widths.png")
print("triangle")
analyse_bin_width(standard, tri_weekly,tri_weekly_half_h, tri_weekly_double_h, tri_monthly, tri_monthly_half_h, tri_monthly_double_h, tri_125,tri_125_half_h, tri_125_double_h, "triangle_ccf_rmse_bin_widths.png")
'''
print("sinc")
analyse_bin_width(standard, sinc_weekly,sinc_weekly_half_h, sinc_weekly_double_h, sinc_monthly, sinc_monthly_half_h, sinc_monthly_double_h, sinc_125,sinc_125_half_h, sinc_125_double_h, "sinc_ccf_rmse_bin_widths.png")
'''
