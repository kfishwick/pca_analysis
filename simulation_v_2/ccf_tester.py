import numpy as np
import matplotlib.pyplot as plt
from ccf_interpolate import ccf

t_min = 0
t_max = 10
t_spacing = 0.01

t_x = np.arange(t_min, t_max, t_spacing)
t_y = t_x

x = np.sin(np.pi*t_x)
y = np.sin(np.pi*t_y)

def sin(t):
	return np.sin(np.pi*t)

fig, (ax_1, ax_2) = plt.subplots(2)
ax_1.plot(t_x,sin(t_x), "k", label= "0 lag")
ax_1.plot(t_x, sin(t_x+0.2), "b--", label= "+0.2 lag")
ax_1.plot(t_x,sin(t_x+0.4), "r--", label= "+0.4 lag")
ax_1.plot(t_x,sin(t_x+0.6), "g--", label= "+0.6 lag")
ax_1.legend()
ax_1.set_xlabel("Time (arb.)")
ax_1.set_ylabel("Signal (arb.)")
max_lag = np.pi
min_lag = -np.pi
lag_nos = np.arange(-300, 300, 1)
lags = lag_nos*t_spacing

ccf_test = ccf(x, x, t_x, t_x, method = "standard", lags=lags, min_weighting = 0,  max_gap=1E5).results()


ax_2.errorbar(lags, ccf_test[:,1], ccf_test[:,2])
ax_2.set_xlabel("lag (arb.)")
ax_2.set_ylabel("ACF")
plt.subplots_adjust(hspace=.3)
plt.savefig("ACF_demo.png")
plt.show()

