import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import vonmises
from sklearn import decomposition
from numpy.random import default_rng

#GLEP_1	55133.1898365
#GLF1_1	2e-15
#snr
snr = 14.0104837417603
period = 405 #ms 

sim_name = "20_trans_125_cad_"

profile_rms = 0.00145841
toa_err = 66.6 #microseconds

#transition paramters
total_no_transitions = 20
transition_cadence = 125
transition_mag = 2e-15
#x axis of profile
profile_x = np.arange(235,275,1)
#von mises definitions for main peak and precursor
narrow_kappa, narrow_peak_x, narrow_scale =  8, 255, 7
wide_kappa_1, wide_peak_x_1, wide_scale_1, wide_kappa_2, wide_peak_x_2, wide_scale_2 = 5, 255, 7, 3, 247,10
#PCA no of components
N=1




def format_transition_epochs(transition_no, transition_epoch, transition_mag):
	print("GLEP_{} {}".format(transition_no, transition_epoch))
	print("GLF1_{} {}".format(transition_no, transition_mag))

def generate_transition_epochs(total_no_transitions, transition_cadence, transition_mag):
	transition_up_epochs = np.array(())
	transition_down_epochs = np.array(())

	for i in range(total_no_transitions):
		if i%2 == 0:
			#format_transition_epochs(i+1, i*transition_cadence, transition_mag)
			transition_up_epochs=np.append(transition_up_epochs, i*transition_cadence)
		elif i%2 == 1:
			#format_transition_epochs(i+1, i*transition_cadence, - transition_mag)
			transition_down_epochs=np.append(transition_down_epochs, i*transition_cadence)
	return transition_up_epochs, transition_down_epochs

def create_profiles(x, kappa_1, peak_x_1, scale_1, kappa_2 = 0, peak_x_2 =0, scale_2 = 0):
	main_comp = vonmises.pdf(x, kappa_1, peak_x_1, scale_1)
	if kappa_2 != 0:
		precursor = vonmises.pdf(x, kappa_2, peak_x_2, scale_2)
		profile = main_comp+precursor
	else:
		profile = main_comp
	return profile
	
def generate_profile_matrix(profile_1, profile_2, transition_epochs_1_to_2, transition_epochs_2_to_1, mjds):
	profile = profile_1 #temp array to store which profile should be added to array
	#find mjds of closest to transition epochs
	transition_2_to_1= []
	for i in range(len(transition_epochs_2_to_1)):
		index = (np.abs(mjds - transition_epochs_2_to_1[i])).argmin()
		transition_2_to_1 = np.append(transition_2_to_1, mjds[index])
	transition_1_to_2= []
	for i in range(len(transition_epochs_1_to_2)):
		index = (np.abs(mjds - transition_epochs_1_to_2[i])).argmin()
		transition_1_to_2 = np.append(transition_1_to_2, mjds[index])
	
	#build profile matrix with transitions
	for i in range(len(mjds)):
		if mjds[i] in transition_1_to_2:
			profile = profile_2
		elif mjds[i] in transition_2_to_1:
			profile = profile_1
		if i==0:
			profile_matrix = profile
		else:
			profile_matrix = np.vstack((profile_matrix, profile))
	return profile_matrix
	
def add_noise(matrix, sn):
	#adds noise to all points on matrix
	for i in range(len(matrix[:,0])):
		peak_value = np.max(matrix[i,:])
		sigma_noise = peak_value/sn 
		new_profile = np.array(())

		noise = np.random.normal(0,profile_rms,len(matrix[i,:])) #gaussian noise
		new_profile = matrix[i,:] + noise
		if i ==0:
			mod_matrix = new_profile
		else:
			mod_matrix = np.vstack((mod_matrix, new_profile)) 
	return mod_matrix  

def FWHM(array):
	#fwhm in terms of bins
	hm = np.max(array)/2
	for i in range(len(array)-1):
		if (array[i]<=hm and array[i+1]>hm) or (array[i]<hm and array[i+1]>=hm):
			w_1 = i
		elif (array[i]>=hm and array[i+1]<hm) or (array[i]>hm and array[i+1]<=hm):
			w_2 = i
	fwhm = (w_2 - w_1)*period/1024
	return fwhm

def TOA_error(SNR, profile_1, profile_2):
	#calc profile widths
	width =	(FWHM(profile_1)+FWHM(profile_2))/2
	return width/SNR 
	
def pca(profile_matrix, N):
	#do pca
	pca = decomposition.PCA(n_components=N)
	pca.fit(profile_matrix)
	projected = pca.transform(profile_matrix)
	return pca.mean_,pca.components_,projected

def random_index_gen(N, n):
	#get indexes to randomly sample
	rng = default_rng()
	max_index = int(N)
	size = int(N*(1 - 1/n))
	indexes = rng.choice(max_index, size=size, replace=False)
	return indexes


def save_data(mjds, coeff, nudot_matrix, reductions, sim_name):
	np.savetxt("coeff_matrix_"+sim_name+".dat", np.column_stack((mjds, coeff)))
	np.savetxt("nudot_matrix_"+sim_name+".dat", nudot_matrix)
	for i in range(len(reductions)):
		remove_indexes=random_index_gen(len(mjds), reductions[i])
		reduced_coeff_matrix = np.delete(np.column_stack((mjds, coeff)), remove_indexes, axis = 0)
		reduced_nudot_matrix = np.delete(nudot_matrix, remove_indexes, axis = 0)
		np.savetxt("coeff_matrix_reduced_{}_".format(reductions[i])+sim_name+".dat", reduced_coeff_matrix)
		np.savetxt("nudot_matrix_reduced_{}_".format(reductions[i])+sim_name+".dat", reduced_nudot_matrix)


def build_data(in_profile_matrix, sim_name, name, removed_indexes, mjds):

	profile_matrix_cut = np.delete(in_profile_matrix, removed_indexes, axis = 0)
	
	mean, comps, coeff = pca(profile_matrix_cut , N)
	if coeff[0]<0:#ensure all coeffs are +vely correlated with nudot
		coeff = -coeff
		comps = -comps
	np.savetxt("coeff_matrix_"+sim_name+name+".dat", np.column_stack((mjds, coeff)))
	
	plt.plot(profile_x, mean, label = "Mean")
	plt.xlabel("bins")
	plt.ylabel("rel. Int. (arb.)")
	plt.title("PCA Components "+name)
	plt.plot(profile_x, comps[0,:], label="1st component")
	plt.legend()
	plt.savefig("simulated_PCA_components_"+name+".png")
	plt.show()
	plt.plot(mjds, coeff)
	plt.xlabel("MJDs")
	plt.ylabel("1st Coeff")
	plt.title("PCA Coefficient "+name)
	plt.savefig("simulated_PCA_coeff_"+name+".png")
	plt.show()

#load in residuals
residuals = np.loadtxt("resids_20_trans_125_cad_error_666.dat")
resid_mjds = residuals[:,0]

#build transitions	
up_trans, down_trans = generate_transition_epochs(total_no_transitions, transition_cadence, transition_mag)

#cut residuals 
#minimum cadence - monthly
remove_indexes_monthly = np.loadtxt("monthly_indexes_to_cut_20_trans_125_cad.dat")
#remove_indexes_monthly = random_index_gen(len(resid_mjds) , 30)
#np.savetxt("monthly_indexes_to_cut_20_trans_125_cad.dat", remove_indexes_monthly)

#monthly_resids = np.delete(residuals, remove_indexes_monthly, axis = 0)
#np.savetxt("monthly_resids_20_trans_125_cad.dat", monthly_resids)
monthly_resids = np.loadtxt("monthly_resids_20_trans_125_cad.dat")

#weekly cadence
#remove_indexes_weekly = random_index_gen(len(resid_mjds) , 7)
remove_indexes_weekly = np.loadtxt("weekly_indexes_to_cut_20_trans_125_cad.dat")
#np.savetxt("weekly_indexes_to_cut_20_trans_125_cad.dat", remove_indexes_weekly)

#weekly_resids = np.delete(residuals, remove_indexes_weekly, axis = 0)
weekly_resids =np.loadtxt("weekly_resids_20_trans_125_cad.dat")
#np.savetxt("weekly_resids_20_trans_125_cad.dat", weekly_resids)

#125 cadence
#remove_indexes_125 = random_index_gen(len(resid_mjds) , 125)
#np.savetxt("125_indexes_to_cut_20_trans_125_cad.dat", remove_indexes_125)
remove_indexes_125 =np.loadtxt("125_indexes_to_cut_20_trans_125_cad.dat")

#resids_125 = np.delete(residuals, remove_indexes_125, axis = 0)
#np.savetxt("125_resids_20_trans_125_cad.dat", resids_125)
resids_125 =np.loadtxt("125_resids_20_trans_125_cad.dat")

#build profiles
narrow_profile = create_profiles(profile_x, narrow_kappa, narrow_peak_x, narrow_scale)
wide_profile = create_profiles(profile_x, wide_kappa_1, wide_peak_x_1, wide_scale_1, wide_kappa_2, wide_peak_x_2, wide_scale_2)
print("TOA error = {} ms".format(TOA_error(snr, wide_profile, narrow_profile)))

#show profiles
plt.plot(profile_x, narrow_profile, label="Narrow")
plt.plot(profile_x, wide_profile, label="Wide")
plt.legend()
plt.show()

#make original profile matrix
profile_matrix = generate_profile_matrix(narrow_profile, wide_profile, up_trans, down_trans, resid_mjds)
noisy_profile_matrix = np.loadtxt("profile_matrix_20_trans_125_cad.dat")

#pca on original profile matrix
mean, comps, coeff = pca(noisy_profile_matrix , N)
if coeff[0]<0:#ensure all coeffs are +vely correlated with nudot
		coeff = -coeff
		comps = -comps
np.savetxt("coeff_matrix_20_trans_125_cad_error_666.dat", np.column_stack((resid_mjds, coeff)))

plt.plot(profile_x, mean, label = "Mean")
plt.xlabel("bins")
plt.ylabel("rel. Int. (arb.)")
plt.title("PCA Components ")
plt.plot(profile_x, comps[0,:], label="1st component")
plt.legend()
plt.savefig("simulated_PCA_components.png")
plt.show()
plt.plot(resid_mjds, coeff)
plt.xlabel("MJDs")
plt.ylabel("1st Coeff")
plt.title("PCA Coefficient")
plt.savefig("simulated_PCA_coeff.png")
plt.show()

#cut profile matrix
build_data(noisy_profile_matrix, "20_trans_125_cad_error_666_","monthly", remove_indexes_monthly, monthly_resids[:,0])
build_data(noisy_profile_matrix, "20_trans_125_cad_error_666_","weekly", remove_indexes_weekly, weekly_resids[:,0])
build_data(noisy_profile_matrix, "20_trans_125_cad_error_666_","125", remove_indexes_125, resids_125[:,0])

"""np.savetxt("nudot_matrix_"+sim_name+".dat", nudot_matrix)
for i in range(len(frac_removed)):
	reduced_nudot_matrix = np.delete(nudot_matrix, remove_indexes[i], axis = 0)
	np.savetxt("nudot_matrix_reduced_{}_".format(frac_removed[i])+sim_name+".dat", reduced_nudot_matrix)
"""

